class Films {

    static create() {
        console.log("Estamos en el metodo crear");
        let nombre = document.querySelector("#nombre").value;
        let genero = document.querySelector("#genero").value;
        let duracion = document.querySelector("#duracion").value;
        let anio = document.querySelector("#anio").value;


        //Creamos objeto vacio
        let myFilm = {};

        //Agregamos par clave/valor
        myFilm.nombre = nombre;
        myFilm.genero = genero;
        myFilm.duracion = duracion;
        myFilm.anio = anio;

        let myFilmJson = JSON.stringify(myFilm);
        Films.show(myFilmJson);
        console.log(myFilmJson);

        Films.send(myFilmJson);
    }

    static send(parametro) {
        fetch("FilsmAdmi",
                {method: "POST",
                    body: parametro
                }
        );
    }

    static show(resultado) {
        document.querySelector("#pnlResultados").innerHTML = "EL resultado ingresado es: " + resultado;
    }

    static main() {
        document.querySelector("#btnCrear").setAttribute("onclick", "Films.create();");
    }
}

Films.main();